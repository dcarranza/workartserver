

CREATE TABLE category_damage (
    id bigint NOT NULL,
    details character varying(255),
    figure character varying(255),
    type character varying(255)
);


A

CREATE TABLE depth_level_damage (
    id bigint NOT NULL,
    color character varying(255),
    name character varying(255)
);



CREATE TABLE painting_type (
    id bigint NOT NULL,
    name character varying(255)
);



CREATE TABLE restoration_cost (
    id bigint NOT NULL,
    cost double precision NOT NULL,
    cat_damage_id bigint NOT NULL,
    lev_damage_id bigint NOT NULL,
    painting_type_id bigint NOT NULL
);
