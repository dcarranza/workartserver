$(".edit").click(function(e){
	e.preventDefault();
	e.stopPropagation();
	
	var url = this.href;
	
	var form = $("#editModal form");
	var inputs = $("#editModal form").find("select, check, input:not([type='button'],[type='submit'])");
	
	$.get(url, function( data ) {

       $.each(data, function(prop, value){
    	   var field;
    		inputs.each(function(){
    			field = $(this);
    			
    			if(prop.toUpperCase()=='ID'){
    				return false;
    			}
    			
    			if(field.attr("name").toUpperCase() == prop.toUpperCase()){
    				   				
    				field.attr("value", value);
    				field.val(value);
    				
    			}    			
    			
    		});
       }); 
       
       form.attr("action",url);
        $('#editModal').modal('show');
        
    }).fail(function(xhr, status, error) {
        
        alert('Error on load data');
    });
});

$(".delete").click(function(e){
	e.preventDefault();
	e.stopPropagation();
	
	var url = this.href;
	var form = $("#deleteModal form");
	
	form.attr("action",url);
    $('#deleteModal').modal('show');
});