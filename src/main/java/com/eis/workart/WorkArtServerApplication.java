package com.eis.workart;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WorkArtServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(WorkArtServerApplication.class, args);
	}

}
