package com.eis.workart.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ConstraintMode;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="restoration_cost", indexes= {@Index(name ="const_indx", unique=true, columnList="painting_type_id, cat_damage_id, lev_damage_id")})

public class RestorationCostEntity {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long Id;
	
	@Column(name="cost", nullable=false)
	private double cost;
	
	@ManyToOne(fetch=FetchType.EAGER, targetEntity=PaintingTypeEntity.class, optional= false, cascade= {CascadeType.REFRESH,CascadeType.PERSIST})
	@JoinColumn(referencedColumnName="id", name="painting_type_id", nullable=false, foreignKey=@ForeignKey(name="fk_restoration_cost_painting_type_id"))
	private PaintingTypeEntity paintingType;
	
	@ManyToOne(fetch=FetchType.EAGER, targetEntity=CategoryDamageEntity.class, optional= false, cascade= {CascadeType.REFRESH,CascadeType.PERSIST})
	@JoinColumn(referencedColumnName="id", name="cat_damage_id", nullable=false, foreignKey=@ForeignKey(name="fk_restoration_cost_cat_damage_id"))
	private CategoryDamageEntity categoryDamage;
	
	@ManyToOne(fetch=FetchType.EAGER, targetEntity=LevelDamageEntity.class, optional= false, cascade= {CascadeType.REFRESH,CascadeType.PERSIST})
	@JoinColumn(referencedColumnName="id", name="lev_damage_id", nullable=false, foreignKey=@ForeignKey(name="fk_restoration_cost_lev_damage_id"))
	private LevelDamageEntity levelDamage;
	
	public RestorationCostEntity() {
		// TODO Auto-generated constructor stub
	}

	public RestorationCostEntity(double cost, PaintingTypeEntity paintingTypeEntity, CategoryDamageEntity categoryDamageEntity,
			LevelDamageEntity levelDamageEntity) {
		super();
		this.cost = cost;
		this.paintingType = paintingTypeEntity;
		this.categoryDamage = categoryDamageEntity;
		this.levelDamage = levelDamageEntity;
	}

	public long getId() {
		return Id;
	}

	public void setId(long id) {
		Id = id;
	}

	public double getCost() {
		return cost;
	}

	public void setCost(double cost) {
		this.cost = cost;
	}

	public PaintingTypeEntity getPaintingType() {
		return paintingType;
	}

	public void setPaintingType(PaintingTypeEntity paintingTypeEntity) {
		this.paintingType = paintingTypeEntity;
	}

	public CategoryDamageEntity getCategoryDamage() {
		return categoryDamage;
	}

	public void setCategoryDamage(CategoryDamageEntity categoryDamageEntiy) {
		this.categoryDamage = categoryDamageEntiy;
	}

	public LevelDamageEntity getLevelDamage() {
		return levelDamage;
	}

	public void setLevelDamage(LevelDamageEntity levelDamageEntity) {
		this.levelDamage = levelDamageEntity;
	}	
	
}
