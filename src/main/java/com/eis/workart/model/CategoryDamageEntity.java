package com.eis.workart.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Table;
import javax.persistence.Id;

@Entity
@Table(name="category_damage")
public class CategoryDamageEntity {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long Id;
	@Column(name="type")
	private String type;
	@Column(name="figure")
	private String figure;
	@Column(name="details")
	private String details;
	
	public CategoryDamageEntity() {
		// TODO Auto-generated constructor stub
	}

	public long getId() {
		return Id;
	}

	public void setId(long id) {
		Id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getFigure() {
		return figure;
	}

	public void setFigure(String figure) {
		this.figure = figure;
	}

	public String getDetails() {
		return details;
	}

	public void setDetails(String details) {
		this.details = details;
	}
	
	
}
