package com.eis.workart.model;

public class CostModel {
	
	private long Id;	

	private double cost;
	
	private long paintingType;

	private long categoryDamage;
	
	private long levelDamage;
	
	public CostModel() {
		// TODO Auto-generated constructor stub
	}	

	public CostModel(long id, double cost, long paintingType, long categoryDamage, long levelDamage) {
		super();
		Id = id;
		this.cost = cost;
		this.paintingType = paintingType;
		this.categoryDamage = categoryDamage;
		this.levelDamage = levelDamage;
	}



	public long getId() {
		return Id;
	}

	public void setId(long id) {
		Id = id;
	}

	public double getCost() {
		return cost;
	}

	public void setCost(double cost) {
		this.cost = cost;
	}

	public long getPaintingType() {
		return paintingType;
	}

	public void setPaintingType(long paintingType) {
		this.paintingType = paintingType;
	}

	public long getCategoryDamage() {
		return categoryDamage;
	}

	public void setCategoryDamage(long categoryDamage) {
		this.categoryDamage = categoryDamage;
	}

	public long getLevelDamage() {
		return levelDamage;
	}

	public void setLevelDamage(long levelDamage) {
		this.levelDamage = levelDamage;
	}
	
	
}
