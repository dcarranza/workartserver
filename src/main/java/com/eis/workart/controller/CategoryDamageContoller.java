package com.eis.workart.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.eis.workart.model.CategoryDamageEntity;
import com.eis.workart.repository.CategoryDamageRepository;

@Controller
@RequestMapping("/category-damage")
public class CategoryDamageContoller {
	
	@Autowired
	private CategoryDamageRepository reporsitory;
	private String home="/category-damage";
	
	@GetMapping
	public String homeCategoryDamage(Model model) {
		
		List<CategoryDamageEntity> list= reporsitory.findAllByOrderByTypeAsc().orElse(new ArrayList<>());;
		model.addAttribute("category_list", list);
		
		return "categorydamage/CategoryDamage";
	}
	
	@GetMapping(path="{Id}")
	@ResponseBody
	public CategoryDamageEntity addCategoryDamage(@PathVariable(name="Id") long id) {
		CategoryDamageEntity entity = reporsitory.findById(id).get();
		
		return entity;
	}
	
	@PostMapping
	public String addCategoryDamage(CategoryDamageEntity entity) {
		
		try {
			if(entity!= null) {
				reporsitory.save(entity);
			}
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		return String.format("redirect:%s", home);
	}
	
	@PutMapping(path="{Id}")
	public String editCategoryDamage(@PathVariable(name="Id") long id, CategoryDamageEntity model) {
		
		CategoryDamageEntity entity = reporsitory.findById(id).get();
		entity.setType(model.getType());
		entity.setDetails(model.getDetails());
		entity.setFigure(model.getFigure());
		if(entity!= null) {
			reporsitory.save(entity);
		} 
		
		return String.format("redirect:%s", home);
	}
	
	@DeleteMapping(path="{Id}")
	public String deleteCategoryDamage(@PathVariable(name="Id") long id) {
		
		reporsitory.deleteById(id);
		return String.format("redirect:%s", home);
	}

}
