package com.eis.workart.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.eis.workart.model.LevelDamageEntity;
import com.eis.workart.repository.DamageLevelRepository;

@Controller
@RequestMapping("/level-damage")
public class LevelDamageController {
	
	@Autowired
	private DamageLevelRepository reporsitory;
	private String home="/level-damage";
	
	@GetMapping
	public String homeLevelDamage(Model model) {
		
		List<LevelDamageEntity> list= reporsitory.findAllByOrderByNameAsc().orElse(new ArrayList<>());;
		model.addAttribute("level_list", list);
		
		return "leveldamage/LevelDamage";
	}
	
	@GetMapping(path="{Id}")
	@ResponseBody
	public LevelDamageEntity addLevelDamage(@PathVariable(name="Id") long id) {
		LevelDamageEntity entity = reporsitory.findById(id).get();
		
		return entity;
	}
	
	@PostMapping
	public String addLevelDamage(LevelDamageEntity entity) {
		try {
			if(entity!= null) {
				reporsitory.save(entity);
			}
		}catch (Exception e) {
			// TODO: handle exception
		}
		
		
		return String.format("redirect:%s", home);
	}
	
	@PutMapping(path="{Id}")
	public String editLevelDamage(@PathVariable(name="Id") long id, LevelDamageEntity model) {
		
		LevelDamageEntity entity = reporsitory.findById(id).get();
		entity.setName(model.getName());
		entity.setColor(model.getColor());
		if(entity!= null) {
			reporsitory.save(entity);
		} 
		
		return String.format("redirect:%s", home);
	}
	
	@DeleteMapping(path="{Id}")
	public String deleteLevelDamage(@PathVariable(name="Id") long id) {
		
		reporsitory.deleteById(id);
		return String.format("redirect:%s", home);
	}

}
