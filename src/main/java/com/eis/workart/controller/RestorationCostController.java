package com.eis.workart.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.eis.workart.model.CategoryDamageEntity;
import com.eis.workart.model.CostModel;
import com.eis.workart.model.LevelDamageEntity;
import com.eis.workart.model.PaintingTypeEntity;
import com.eis.workart.model.RestorationCostEntity;
import com.eis.workart.repository.CategoryDamageRepository;
import com.eis.workart.repository.DamageLevelRepository;
import com.eis.workart.repository.PaintingTypesRepository;
import com.eis.workart.repository.RestorationCostRepository;

@Controller
@RequestMapping("/restoring-cost")
public class RestorationCostController {

	private String home="/restoring-cost";
	
	@Autowired
	private RestorationCostRepository reporsitory;
	
	@Autowired
	private PaintingTypesRepository paintingRepository;
	
	@Autowired
	private CategoryDamageRepository categoryRepository;
	
	@Autowired
	private DamageLevelRepository levelRepository;
	
	@GetMapping
	public String homeRestorationCost(Model model, RestorationCostEntity entity) {
		
		List<RestorationCostEntity> list = reporsitory.findAll();
		List<PaintingTypeEntity> listPainting = paintingRepository.findAllByOrderByNameAsc().orElse(new ArrayList<>());
		List<CategoryDamageEntity> listCategory = categoryRepository.findAllByOrderByTypeAsc().orElse(new ArrayList<>());
		List<LevelDamageEntity> listLevel = levelRepository.findAllByOrderByNameAsc().orElse(new ArrayList<>());
		
		model.addAttribute("cost_list", list);
		model.addAttribute("painting_list", listPainting);
		model.addAttribute("category_list", listCategory);
		model.addAttribute("level_list", listLevel);
		model.addAttribute("newCost",entity);
		
		return "restorationscost/RestorationCost";
	}
	
	@GetMapping(path="{Id}")
	@ResponseBody
	public CostModel addRestorationCost(@PathVariable(name="Id") long id) {
		
		RestorationCostEntity entity = reporsitory.findById(id).get();
		CostModel model = new CostModel();
		model.setCategoryDamage(entity.getCategoryDamage().getId());
		model.setLevelDamage(entity.getLevelDamage().getId());
		model.setPaintingType(entity.getPaintingType().getId());
		model.setCost(entity.getCost());
		
		return model;
	}
	
	@PostMapping
	public String addRestorationCost(CostModel model) {
		
		PaintingTypeEntity paintingType = paintingRepository.findById(model.getPaintingType()).get();
		LevelDamageEntity levelDamage = levelRepository.findById(model.getLevelDamage()).get();
		CategoryDamageEntity categoryDamage = categoryRepository.findById(model.getCategoryDamage()).get();
		
		RestorationCostEntity entity = new RestorationCostEntity(model.getCost(), paintingType, categoryDamage, levelDamage);
		try {
			reporsitory.save(entity);
		}catch (Exception e) {
			// TODO: handle exception
			System.out.println(e.getMessage());
		}
		
		
		return String.format("redirect:%s", home);
	}
	
	@PutMapping(path="{Id}")
	public String editRestorationCost(@PathVariable(name="Id") long id, CostModel model) {
		
		RestorationCostEntity entity = reporsitory.findById(id).get();
		PaintingTypeEntity paintingType = paintingRepository.findById(model.getPaintingType()).get();
		LevelDamageEntity levelDamage = levelRepository.findById(model.getLevelDamage()).get();
		CategoryDamageEntity categoryDamage = categoryRepository.findById(model.getCategoryDamage()).get();
		
		entity.setCategoryDamage(categoryDamage);
		entity.setLevelDamage(levelDamage);
		entity.setPaintingType(paintingType);
		entity.setCost(model.getCost());
		
		reporsitory.save(entity);
		return String.format("redirect:%s", home);
	}
	
	@DeleteMapping(path="{Id}")
	public String deleteRestorationCost(@PathVariable(name="Id") long id) {
		
		reporsitory.deleteById(id);
		return String.format("redirect:%s", home);
	}
}
