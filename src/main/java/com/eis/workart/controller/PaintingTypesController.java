package com.eis.workart.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.eis.workart.model.PaintingTypeEntity;
import com.eis.workart.repository.PaintingTypesRepository;

@Controller
@RequestMapping("/painting-types")
public class PaintingTypesController {
	
	@Autowired
	PaintingTypesRepository reporsitory;
	
	@GetMapping
	public String homePaintingTypes(Model model) {
		
		List<PaintingTypeEntity> list= reporsitory.findAllByOrderByNameAsc().orElse(new ArrayList<>());
		model.addAttribute("painting_list", list);
		return "paintingstype/PaintingTypes";
	}
	
	@GetMapping(path="{Id}")
	@ResponseBody
	public PaintingTypeEntity addPaintingTypes(@PathVariable(name="Id") long id) {
		PaintingTypeEntity entity = reporsitory.findById(id).get();
		
		return entity;
	}
	
	@PostMapping
	public String addPaintingType(PaintingTypeEntity entity) {
		
		try {
			if(entity!= null) {
				reporsitory.save(entity);
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		
		return "redirect:/painting-types";
	}
	
	@PutMapping(path="{Id}")
	public String editPaintingType(@PathVariable(name="Id") long id, PaintingTypeEntity model) {
		
		PaintingTypeEntity entity = reporsitory.findById(id).get();
		entity.setName(model.getName());
		
		if(entity!= null) {
			reporsitory.save(entity);
		} 
		
		return "redirect:/painting-types";
	}
	
	@DeleteMapping(path="{Id}")
	public String deletePaintingType(@PathVariable(name="Id") long id) {
		
		reporsitory.deleteById(id);
		return "redirect:/painting-types";
	}
	
	
}
