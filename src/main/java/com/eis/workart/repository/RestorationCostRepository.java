package com.eis.workart.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.eis.workart.model.RestorationCostEntity;

public interface RestorationCostRepository extends JpaRepository<RestorationCostEntity, Long> {
	
	
}
