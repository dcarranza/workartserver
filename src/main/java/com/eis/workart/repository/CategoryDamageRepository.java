/**
 * 
 */
package com.eis.workart.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.eis.workart.model.CategoryDamageEntity;
import com.eis.workart.model.LevelDamageEntity;

/**
 * @author acer
 *
 */
public interface CategoryDamageRepository extends JpaRepository<CategoryDamageEntity, Long> {
	
	public Optional<List<CategoryDamageEntity>> findAllByOrderByTypeAsc();
}
