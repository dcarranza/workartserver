package com.eis.workart.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.eis.workart.model.PaintingTypeEntity;

public interface PaintingTypesRepository extends JpaRepository<PaintingTypeEntity, Long> {
	
	public Optional<List<PaintingTypeEntity>> findAllByOrderByNameAsc();
}
