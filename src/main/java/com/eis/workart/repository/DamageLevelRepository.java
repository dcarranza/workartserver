package com.eis.workart.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.eis.workart.model.LevelDamageEntity;
import com.eis.workart.model.PaintingTypeEntity;

public interface DamageLevelRepository extends JpaRepository<LevelDamageEntity, Long> {

	public Optional<List<LevelDamageEntity>> findAllByOrderByNameAsc();
}
